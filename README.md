# tdCalendarWrap

#### 介绍
基于jquery的日期选择框架

#### 软件架构
jquery

#### 使用说明

1.html标签

```
<div class="container">
    <div id="tdCalendarWrap"></div>
</div>
```

2. 插件初始化

```
$("#tdCalendarWrap").tdCalendarWrap({
    // 已选择的日期 初始化值
    selectedDate: ["2021-03-18", "2021-03-19", "2021-04-01"],
    // 最大选择数量
    selectedSizeMax: 4,
            // 可选择的日期 null 表示都可选  直接传日期 或 对象中传都ok
            optionalDate: [
    "2021-03-17",
            "2021-03-18",
            "2021-03-19", {
            date: "2021-03-22",
                }, {
        date: "2021-03-25",
    }, {
        date: "2021-04-01",
                // 上面展示的字
                upText: "愚人节",
                // 下面展示的字
                downText: "哈哈",
                // 下面展示的字
                selectDownText: "嘿嘿",
    }, {
        date: "2021-05-01",
                // 上面展示的字
                upText: "劳动节",
                // 下面展示的字
                downText: "劳动",
                // 下面展示的字
                selectDownText: "加油",
    }],
    // 可选日期配置
    optionalDateConfig: {
        // 下面展示的字
        downText: "抢",
                // 下面展示的字
                selectDownText: "多选",
    },
    // 显示开始月份
    showStartYearMouth: "2021-03",
            // 显示结束月份 默认一年
            showEndYearMouth: "2021-05",
            // 当选择后执行的回调
            // clickDate 当前点击的日期对象
            // selectedDate 全部选择的日期对象列表
            // elem 点击的dom元素
            done: function (clickDate, selectedDate, elem) {
        console.log("selectedDate", selectedDate.join(','));
        getTdCalendarWrapSelectDate();
    },
    // 当选择数量超过后的回调后执行的回调
    // clickDate 当前点击的日期对象
    // selectedSize 已选择的数量
    // elem 点击的dom元素
    selectedSizeExceed: function (clickDate, selectedSize, elem) {
        alert("对多选择" + selectedSize + "个日期");
    }
});
```

3. 获取选择的日期

```
var selectedDate = $("#tdCalendarWrap").tdCalendarWrap("getSelectedDate");
console.log(selectedDate);
```


